public class Main {

    public static void main(String[] args) {
        BlockChain blockChain = new BlockChain();

        blockChain.addBLock(blockChain.newBlock("Data1"));
        blockChain.addBLock(blockChain.newBlock("Data2"));
        blockChain.addBLock(blockChain.newBlock("Data3"));

        System.out.println("Blockchain valide ? " + blockChain.isBlockChainValid());
        System.out.println(blockChain);

        blockChain.addBLock(new Block(15, "Data4", "00000792ef3e0fae8415f8f8ed7b2ae0fca2459dbec657c24eec4b740e78985a"));

        System.out.println("Blockchain valide ? " + blockChain.isBlockChainValid());
        System.out.println(blockChain);
    }   
}