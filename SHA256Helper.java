import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utilitaire permettant d'hasher une chaine de caractère comportant des données à crypter
 * avec l'aide de l'algorithme SHA-256 sur un format hexa de 32-digit
 */
public class SHA256Helper {
    
    public static String generateHash(String dataToHash) {
        try {
            // On récupère une instance de SHA-256. Ensemble de fonction en cryptographie.
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            // Génération du hash qui résulte en un tableau de byte
            byte[] hash = digest.digest(dataToHash.getBytes("UTF-8"));

            // Transformation du hash en hexadecimal.
            // Un hash est généralement une représentation d'un nombre hexadecimal à 32 chiffres
            StringBuffer hexadecimalString = new StringBuffer();  
            for (int i = 0; i < hash.length; i++) {
                String hexadecimal = Integer.toHexString(0xff & hash[i]);

                if (hexadecimal.length() == 1) {
                    hexadecimalString.append('0');
                }
                hexadecimalString.append(hexadecimal);
            }
            return hexadecimalString.toString();
        } catch (NoSuchAlgorithmException|UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
