public class Utils {

    public static String zeros(Integer length) {
        StringBuilder builder = new StringBuilder();

        for (Integer i = 0; i < length; i++) {
            builder.append('0');
        }

        return builder.toString();
    }    
}
