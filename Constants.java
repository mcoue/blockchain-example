/**
 * Représentation des constantes utilisées dans la blockchaine telles que
 * - la difficulté pour les mineurs à trouver le hash
 * - la récompense pour les mineurs à avoir trouver le hash
 * - le hash par défaut pour le premier bloque de la blockchaine
 */
public class Constants {
    
    /**
     * Définit la difficulté des mineurs à trouver le bon hash
     * 1 veut dire qu'il y aura 1 zero au début du hash
     */
    public static final Integer DIFFICULTY = 5;
    /**
     * Définit la récompense donnée au mineur quand il trouve le bon hash
     */
    public static final Double MINER_REWARD = 10D;
    /**
     * Connaissant la strucutre de la blockchain, le bloque initial n'aura donc pas de bloque antérieur à lui et donc, par conséquent, pas de hash antérieur
     */
    public static final String GENESIS_PREV_HASH = "0000000000000000000000000000000000000000000000000000000000000000";  
}