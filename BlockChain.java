import java.util.ArrayList;
import java.util.List;

/**
 * Strucutre représentative d'une blockchain.
 * Une blockchain se représente par une succéssion de {@link Block}
 */
public class BlockChain {

    /**
     * La liste des bloques de la blockchain
     * Et par définition qui est la blockchain en elle-même
     */
    private List<Block> blockChain;
    private Integer difficulty;

    public BlockChain() {
        this.difficulty = Constants.DIFFICULTY;
        this.blockChain = new ArrayList<>();

        Block b = new Block(0, null, Constants.GENESIS_PREV_HASH);
        b.mineBlock(difficulty);
        blockChain.add(b);
    }

    /**
     * Détermine si le premier bloque de la blockchain respecte le contrat et 
     * donc pas conséquent, détermine si le bloque est valide
     * 
     * @return true si le premier bloque de la blockchain est valide
     */
    public boolean isFirstBlockValid() {
        Block firstBlock = blockChain.get(0);

        if (firstBlock.getIndex() != 0) {
            return false;
        }

        if (firstBlock.getPreviousHash() != Constants.GENESIS_PREV_HASH) {
            return false;
        }

        if (firstBlock.getHash() == null || !Block.calculerHash(firstBlock).equals(firstBlock.getHash())) {
            return false;
        }

        return true;
    }

    /**
     * Permet de savoir si le bloque courant est valide
     * 
     * @param newBlock le block a valider
     * @param previousBlock le précédent bloque valide
     * @return true si le block est valide
     */
    public boolean isValidNewBlock(Block newBlock, Block previousBlock) {
        if (newBlock != null && previousBlock != null) {
            if (previousBlock.getIndex() + 1 != newBlock.getIndex()) {
                return false;
            }

            if (newBlock.getPreviousHash() == null || !newBlock.getPreviousHash().equals(previousBlock.getHash())) {
               return false;
            }

            if (newBlock.getHash() == null || !Block.calculerHash(newBlock).equals(newBlock.getHash())) {
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Permet de connaître la validité de la blockchain
     *
     * @return true si la blockchain est valide
     */
    public boolean isBlockChainValid() {
        if (!isFirstBlockValid()) {
            return false;
        }

        for (int i = 1; i < blockChain.size(); i++) {
            Block currentBlock = blockChain.get(i);
            Block previousBlock = blockChain.get(i - 1);

            if (!isValidNewBlock(currentBlock, previousBlock)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Récupère le dernier bloque connu de la blockchain
     * 
     * @return le block
     */
    public Block getLatestBlock() {
        return blockChain.get(blockChain.size() - 1);
    }

    /**
     * Permet de créer un nouveau block
     * 
     * @param data Les données du block
     */
    public Block newBlock(String data) {
        Block latestBlock = getLatestBlock();
        return new Block(latestBlock.getIndex() + 1, data, latestBlock.getHash());
    }

    public void addBLock(Block b) {
        if (b != null) {
            b.mineBlock(difficulty);
            blockChain.add(b);
        }
    }

    /**
     * Permet de récupérer la blockchain
     * 
     * @return la blockchain
     */
    public List<Block> getBlockChain() {  
        return this.blockChain;  
    }  

    /**
     * Permet de récupérer la taille de la blockchain
     * 
     * @return la taille de la blockchain
     */
    public Integer size() {
        return this.blockChain.size();
    }

    @Override  
    public String toString() {  
        StringBuilder blockChain = new StringBuilder(); 
        for(Block block : this.blockChain) {
            blockChain.append(block.toString());
            blockChain.append("\n");  
        } 
        return blockChain.toString();  
    }  
}
