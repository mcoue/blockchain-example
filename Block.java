/**
 * Structure représentative d'un bloque d'une blockchain
 */
public class Block {
    
    /**
     * Le numéro du bloque
     */
    private Integer index;
    /**
     * Un noeud qui est un nombre arbitraire utilisé en cryptographie
     */
    private Integer nonce;
    /**
     * Le timestamp de la création du bloque
     */
    private Long timeStamp;
    /**
     * Le hash du bloque courant basé sur son contenu
     */
    private String hash;
    /**
     * Hash du bloque précédent
     */ 
    private String previousHash;
    /**
     * Les données actuelles du bloque
     */
    private String data;

    public Block (Integer index, String data, String previousHash) {
        this.index = index;
        this.nonce = 0;
        this.data = data;
        this.previousHash = previousHash;
        this.timeStamp = System.currentTimeMillis();
        this.hash = Block.calculerHash(this);
    }

    /**
     * Définit les données qui devront être hashées
     * 
     * @return les données à hash
     */
    public String getDataToHash() {
        return index + previousHash + timeStamp + nonce + data;
    }

    /**
     * Incremente de 1 le noeud actuel
     */
    public void incrementNonce() {
        this.nonce++;
    }

    /**
     * Permet de remettre le compteur à 0 lorsque le hash a été miné
     */
    public void resetNonce() {
        this.nonce = 0;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getNonce() {
        return nonce;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String getData() {
        return data;
    }

    public static String calculerHash(Block block) {
        return SHA256Helper.generateHash(block.getDataToHash());
    }

    public void mineBlock(Integer difficulty) {
        nonce = 0;

        while (!getHash().substring(0, difficulty).equals(Utils.zeros(difficulty))) {
            nonce++;
            this.hash = Block.calculerHash(this);
        }
    }

    @Override
    public String toString() {
        return this.index + "-" + this.data + "-" + this.hash + "-" + this.previousHash + "-";
    }    
}
